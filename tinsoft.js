require('dotenv').config();
const TinSoftProxy = require('tinsoftproxy');
const request = require('request');
var ProxyAgent = require('proxy-agent');

const proxyService = new TinSoftProxy({
  user_api_key: process.env.TINSOFT_USER_API_KEY,
  api_key: process.env.TINSOFT_API_KEY
});

async function f() {
  try {
    var i = 0, howManyTimes = process.env.TIME_LOOP;
    console.log("Begin......");
    const rp = await proxyService.pickup({
      api_key: process.env.TINSOFT_API_KEY || '',
      random: false,
      location_id: 0
    })
    // .then(rp => {
    //   console.log('rp:', rp);
    //   const proxyUri = `http://${rp.proxy}`
    
    //   const agent = new ProxyAgent(proxyUri)
    
    //   request({
    //     timeout: 3000,
    //     url: process.env.URL_REQUEST,
    //     json: true,
    //     // proxy: proxyUri,
    //     agent
    //   }, (err, res, body) => {
    //     if (!err) {
    //       console.log("Successfully request" + process.env.URL_REQUEST);
    //       console.log("Waiting time for re-request: " + delay/1000 + "s");
    //     } else {
    //       console.log(err);
    //     }
    //   })
    // });   
    console.log('rp:', rp);
    const proxyUri = `http://${rp.proxy}`

    const agent = new ProxyAgent(proxyUri)
    var delay = rp.next_change * 1000;
    request({
      timeout: 3000,
      url: process.env.URL_REQUEST,
      json: true,
      // proxy: proxyUri,
      agent
    }, (err, res, body) => {
      if (!err) {
        console.log("Successfully request" + process.env.URL_REQUEST);
        console.log("Waiting time for re-request: " + delay/1000 + "s");
      } else {
        console.log(err);
      }
    })
    // i ++
    i++;
    if (i < howManyTimes) {
      setTimeout(f, delay);
    }
  } catch (e){
    console.log(e);
  }
}

f();